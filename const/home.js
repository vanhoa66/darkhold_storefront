const BOOK_TOP = [
  {
    imageURL:
      'https://images.prismic.io/wonderbly/c500889a-a5a0-4517-a1b9-3373ddb1ab43_ODS_HP_feature_partial_desktop.jpg',
    title: 'My Daddy The Superhero',
    desc: 'Personalized for super dads!',
    path: '/#',
  },
  {
    imageURL:
      'https://images.prismic.io/wonderbly/1c4a5d9c-7081-4ef0-bc7f-f6b945077034_ILY_Dad_homepage_feature_cards_desktop.jpg',
    title: 'I Love Daddy This Much',
    desc: 'Perfect Fathers Day gift',
    path: '/#',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/1bff44fb-aeb7-4e3c-b9cd-083f4b1a665e_LMN_features_desktop.jpg',
    title: 'Lost My Name',
    desc: 'A story of self-discovery',
    path: '/#',
  },
]
const BOOK_AGE = [
  {
    imageURL: 'https://images.prismic.io/wonderbly/432b243b433e6d95261854c9ef2a7047b2083140_0-3_01d-1.png',
    title: 'Age 0 to 3',
    desc: 'Stories of love and learning',
    path: '/#',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/0c3061dfd9ef693a14ff910ce1bf28b63b06f87f_3-6_01d-2.png',
    title: 'Age 3 to 6',
    desc: 'Imagination-boosting books',
    path: '/#',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/99b60d272cc381cd051bb10ea870c88974a58825_6_01d-2.png',
    title: 'Age 6 and up',
    desc: 'Books to grow their confidence',
    path: '/#',
  },
  {
    imageURL:
      'https://images.prismic.io/wonderbly/47ca40f9-fb7b-44c0-8db2-03c8b769f81a_2021_Grownups_Range_Partial_desktop.jpg',
    title: 'Grown-ups',
    desc: 'Unique gifts for your favorite people',
    path: '/#',
  },
]
const BOOK_BESTSELLING = [
  {
    badge: {
      name: 'Personalized love book',
      backgroundColor: '#FFCC37',
    },
    imageURL: 'https://images.prismic.io/wonderbly/ab3075f6-07c2-4411-bf3d-a4994d4472cb_01_Thumbnail_ILY_Generic.png',
    title: 'I Love You This Much',
    desc: 'A story of limitless love, made for up to 4 children.',
    age: 'Age 0-3',
    price: 'From $34.99 USD',
    path: '/#',
  },
  {
    badge: {
      name: 'Worldwide best seller',
      backgroundColor: '#F27860',
    },
    imageURL: 'https://images.prismic.io/wonderbly/0fc94ef5-21ec-4d7e-a972-d5bb647157ce_YIFY_rangepage.png',
    title: 'ABC For You',
    desc: 'Introduce a child to the alphabet, and kickstart their reading journey with the power of personalization. The ABCs will never be boring again!',
    age: 'Age 0-3',
    price: 'From $34.99 USD',
    path: '/#',
  },
  {
    badge: {
      name: 'Personalized love book',
      backgroundColor: '#F6707B',
    },
    imageURL: 'https://images.prismic.io/wonderbly/7930d1b0-443b-448d-a794-c5c262ec888f_01_Thumbnail_NSFY.png',
    title: 'I Love You This Much',
    desc: 'A story of limitless love, made for up to 4 children.',
    age: 'Age 0-3',
    price: 'From $34.99 USD',
    path: '/#',
  },
]
const FAQ_DATA = [
  {
    idHeading: 'headingOne',
    idCollapse: 'collapseOne',
    title: 'Who is Wonderbly?',
    content:
      'We’re so glad you asked! Wonderbly is an online personalized children and adults book company. Since 2013, our bestselling books have delighted over six million children (and grown-ups!) all over the world. Together, we want to inspire the next generation of brave, imaginative, book-loving kids – one personalized story at a time.',
  },
  {
    idHeading: 'headingTwo',
    idCollapse: 'collapseTwo',
    title: 'How do I make a personalized book?',
    content:
      'We’re so glad you asked! Wonderbly is an online personalized children and adults book company. Since 2013, our bestselling books have delighted over six million children (and grown-ups!) all over the world. Together, we want to inspire the next generation of brave, imaginative, book-loving kids – one personalized story at a time.',
  },
  {
    idHeading: 'headingThree',
    idCollapse: 'collapseThree',
    title: 'How do I place an order with Wonderbly?',
    content:
      'We’re so glad you asked! Wonderbly is an online personalized children and adults book company. Since 2013, our bestselling books have delighted over six million children (and grown-ups!) all over the world. Together, we want to inspire the next generation of brave, imaginative, book-loving kids – one personalized story at a time.',
  },
  {
    idHeading: 'headingFour',
    idCollapse: 'collapseFour',
    title: 'How much are Wonderbly books?',
    content:
      'We’re so glad you asked! Wonderbly is an online personalized children and adults book company. Since 2013, our bestselling books have delighted over six million children (and grown-ups!) all over the world. Together, we want to inspire the next generation of brave, imaginative, book-loving kids – one personalized story at a time.',
  },
]
const FEATURE_HIGHLIGHT = [
  {
    imageURL: 'https://images.prismic.io/wonderbly/d8477ea255f7c264a75b7d23f1d77a5df3f97a9a_star-03.png',
    title: 'Rated 5 stars',
    desc: 'Trustpilot have rated us as excellent. Hooray!',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/16f8de261ec0f067378ef5c3b968b5072bb77604_dedication-03.png',
    title: 'Free dedication',
    desc: 'Make it extra personal, with a loving message',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly%2F6359f5ce-5192-4331-8bb0-357fe55bb8a2_quality-03.png',
    title: '6 million sold',
    desc: 'Our books have delighted oodles of children',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/e47502bc0b063a0b43b11d0a304f5bc0a64b8260_keepsake-04.png',
    title: '97% satisfaction',
    desc: 'Our customers think we re pretty special!',
  },
]
const FEATURE_GIFT = [
  {
    imageURL: 'https://images.prismic.io/wonderbly/3062ade5424906990f3ac0190e2132a8222e4544_shipping_alt-03.png',
    title: 'Ship worldwide',
    desc: 'Printed and shipped within 48 hours',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/7afe6bd48348f3ef661d9a8d68187fdd84da4289_language-03.png',
    title: '12 languages',
    desc: 'Children across the world can enjoy our stories',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly%2Fb08be445-e4a0-4361-9160-808e28e3d10f_keepsake-04.png',
    title: 'Superb quality',
    desc: 'Lovingly made books, to be loved for years',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/b6d9193cd7b1c869184779f00483ed07dbb4d553_unique-03.png',
    title: 'Personalized',
    desc: 'Uniquely personalized stories, like no others',
  },
]
export { BOOK_TOP, BOOK_AGE, BOOK_BESTSELLING, FAQ_DATA, FEATURE_HIGHLIGHT, FEATURE_GIFT }
