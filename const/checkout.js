const FOOTER_CHECKOUT = [
  {
    icon: 'paper-plane',
    title: 'Delivery',
    desc: 'Each book is printed especially for you and takes 48 hours to make. Delivery time is added on top.',
  },
  {
    icon: 'repeat',
    title: 'Returns',
    desc: "Our books are wonderfully personalized so we can't always offer refunds or exchanges. However you'll have 12-24 hours after ordering to review & make edits.",
  },
  {
    icon: 'comment',
    title: 'Help',
    desc: 'Send us a message or find an answer on our super duper help center',
  },
]

export { FOOTER_CHECKOUT }
