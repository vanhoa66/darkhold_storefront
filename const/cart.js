const CART_PORDUCT = [
  {
    id: '1',
    imageURL:
      'https://mu2.wonderbly.com/product/you-love-daddy-this-much-v2/preview/front/front-right?locale=en-US&gifters%5B0%5D%5Bname%5D=Daddy&gifters%5B0%5D%5Bgender%5D=male&characters%5B0%5D%5Bname%5D=Splendid&characters%5B0%5D%5Bgender%5D=girl&characters%5B0%5D%5Bphototype%5D=type-ii&story_type=story-ii&cover_style=cover-ii&character_count=1&gifter_count=1&_q=0.8&_v=208107d7&_size=230x230x5&_sig=5gEQX4mnM_8zy9QM6yPSrHso0bjGcl9qAZ4a1sJwWvU&_width=958',
    title: 'I Love Daddy This Much for Splendid',
    category: 'Softcover',
    gifts: {
      show: true,
      options: [
        {
          title: 'Edit book',
          icon: 'pencil',
          path: '/#',
        },
        {
          title: 'Edit gift wrap',
          icon: 'gift',
          path: '/#',
        },
        {
          title: 'Add other extras',
          icon: 'star',
          path: '/#',
        },
      ],
    },
    btn: {
      show: false,
    },
    price: 19.22,
  },
  {
    id: '2',
    imageURL:
      'https://mu2.wonderbly.com/product/you-love-grandad-this-much-v2/image/cover?locale=en-US&gifters[0][name]=Grandad&gifters[0][gender]=male&&characters[0][name]=Ghjghj&characters[0][gender]=boy&characters[0][phototype]=type-ii&&story_type=story-ii&cover_style=cover-iii&inscription=Dear Grandad,In every tiny thing you do each day, never forget that you are loved enormously.&box=no-box&cover=hardback&size=super-square&_width=958',
    title: 'I Love Daddy This Much for Splendid',
    category: 'Softcover',
    gifts: {
      show: false,
    },
    btn: {
      show: false,
    },
    price: 62.45,
  },

  {
    id: '3',
    imageURL:
      'https://mu2.wonderbly.com/product/you-love-daddy-this-much-v2/preview/front/front-right?locale=en-US&gifters%5B0%5D%5Bname%5D=Daddy&gifters%5B0%5D%5Bgender%5D=male&characters%5B0%5D%5Bname%5D=Splendid&characters%5B0%5D%5Bgender%5D=girl&characters%5B0%5D%5Bphototype%5D=type-ii&story_type=story-ii&cover_style=cover-ii&character_count=1&gifter_count=1&_q=0.8&_v=208107d7&_size=230x230x5&_sig=5gEQX4mnM_8zy9QM6yPSrHso0bjGcl9qAZ4a1sJwWvU&_width=958',
    title: 'I Love Daddy This Much for Splendid',
    category: 'Softcover',
    gifts: {
      show: false,
    },
    btn: {
      show: false,
    },
    price: 22.45,
  },
]

const CART_PORDUCT_UPSELL = [
  {
    id: '4',
    imageURL:
      'https://mu2.wonderbly.com/product/you-love-daddy-this-much-v2/preview/front/front-right?locale=en-US&gifters%5B0%5D%5Bname%5D=Daddy&gifters%5B0%5D%5Bgender%5D=male&characters%5B0%5D%5Bname%5D=Splendid&characters%5B0%5D%5Bgender%5D=girl&characters%5B0%5D%5Bphototype%5D=type-ii&story_type=story-ii&cover_style=cover-ii&character_count=1&gifter_count=1&_q=0.8&_v=208107d7&_size=230x230x5&_sig=5gEQX4mnM_8zy9QM6yPSrHso0bjGcl9qAZ4a1sJwWvU&_width=958',
    title: 'I Love Daddy This Much for Splendid',
    category: 'Get the set and save!',
    btn: {
      show: true,
      text: 'See the book',
      path: '/#',
    },
    gifts: {
      show: false,
    },
    price: 39.48,
  },
]

export { CART_PORDUCT, CART_PORDUCT_UPSELL }
