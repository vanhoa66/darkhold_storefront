const MENU_FOOTER = [
  {
    name: 'Wonderbly',
    children: [
      {
        name: 'Our story',
        path: '/#',
      },
      {
        name: 'Blog',
        path: '/#',
      },
      {
        name: 'Careers',
        path: '/#',
      },
      {
        name: 'Press',
        path: '/#',
      },
      {
        name: 'Offers',
        path: '/#',
      },
      {
        name: 'Affiliate Programme',
        path: '/#',
      },
      {
        name: 'Get a $15 reward',
        path: '/#',
      },
    ],
  },
  {
    name: 'Shop with us',
    children: [
      {
        name: 'All Personalized Books',
        path: '/#',
      },
      {
        name: 'World of Lost My NameBestseller',
        path: '/#',
      },
      {
        name: 'World of Where Are You',
        path: '/#',
      },
      {
        name: 'Books for babies & newborn',
        path: '/#',
      },
      {
        name: 'Books for toddlers',
        path: '/#',
      },
      {
        name: 'Books for children ages 3-6',
        path: '/#',
      },
      {
        name: 'Books for children ages 6+',
        path: '/#',
      },
      {
        name: 'First Steps For You',
        path: '/#',
      },
      {
        name: 'Christmas books',
        path: '/#',
      },
      {
        name: 'Activity books for kids',
        path: '/#',
      },
      {
        name: 'Mothers day books',
        path: '/#',
      },
      {
        name: 'Birthday books',
        path: '/#',
      },
      {
        name: 'All Personalized Books',
        path: '/#',
      },
      {
        name: 'Books for siblings',
        path: '/#',
      },
      {
        name: 'Easter Books',
        path: '/#',
      },
      {
        name: 'Family Books',
        path: '/#',
      },
      {
        name: 'Baby shower books',
        path: '/#',
      },
      {
        name: 'Personalized anniversary books',
        path: '/#',
      },
      {
        name: 'All Personalized Books',
        path: '/#',
      },
      {
        name: 'Personalized Christmas card',
        path: '/#',
      },
      {
        name: 'Personalized card for Mom',
        path: '/#',
      },
      {
        name: 'Counting activity sheet',
        path: '/#',
      },
      {
        name: 'Rainbow activity sheet',
        path: '/#',
      },
      {
        name: 'Alphabet activity sheet',
        path: '/#',
      },
    ],
  },
  {
    name: 'Help & Support',
    children: [
      {
        name: 'Contact Us',
        path: '/#',
      },
      {
        name: 'Delivery Info',
        path: '/#',
      },
      {
        name: 'Returns Policy',
        path: '/#',
      },
      {
        name: 'Privacy Policy',
        path: '/#',
      },
      {
        name: 'UGC Terms and Conditions',
        path: '/#',
      },
      {
        name: 'Sitemap',
        path: '/#',
      },
    ],
  },
]
const PAYMENT_IMAGE = [
  {
    imageURL: 'https://images.prismic.io/wonderbly/64950be3ee572911f8b370a02c4df28f7532b3a8_amazon.png',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/98a189995b5d72d8e30fc34d323bfc9f61cb6c76_visa.png',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/c783f122edccd5b5219bcc91c701c47268e853fa_mastercard.png',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/98a189995b5d72d8e30fc34d323bfc9f61cb6c76_visa.png',
  },
  {
    imageURL: 'https://images.prismic.io/wonderbly/617394c088e2c5a2bfeb9d81dde131dfb675b72b_paypal.png',
  },
]
const SOCIAL_ICON = [
  {
    name: 'facebook',
  },
  {
    name: 'twitter',
  },
  {
    name: 'instagram',
  },
  {
    name: 'pinterest',
  },
]
const TEXT_FOOTER_BOTTOM = [
  {
    name: 'Terms & Conditions',
    path: '/#',
  },
  {
    name: 'Privacy Policy',
    path: '/#',
  },
  {
    name: '30-36 Pritchards Rd, London E2 9AP',
  },
  {
    name: 'Lostmy.name Ltd (trading as Wonderbly, Reg Co. Number 08305498)',
  },
]

export { MENU_FOOTER, PAYMENT_IMAGE, SOCIAL_ICON, TEXT_FOOTER_BOTTOM }
