const menus = [
  {
    name: 'Collections',
    path: '/collections',
    icon: {
      name: 'book',
      color: 'red',
    },
    children: [],
  },
  {
    name: 'Books by age',
    icon: {
      name: 'bookmark',
      color: 'blue',
    },
    children: [
      {
        imageURL: 'https://images.prismic.io/wonderbly/f2c493de48bb1501ff672cfd61c7bb55bdd97a7f_0-3.nav.png',
        title: 'Where Are You ...?',
        path: '/books',
      },
      {
        imageURL: 'https://images.prismic.io/wonderbly/f2c493de48bb1501ff672cfd61c7bb55bdd97a7f_0-3.nav.png',
        title: 'Where Are You ...?',
        path: '/#',
      },
      {
        imageURL: 'https://images.prismic.io/wonderbly/f2c493de48bb1501ff672cfd61c7bb55bdd97a7f_0-3.nav.png',
        title: 'Where Are You ...?',
        path: '/#',
      },
      {
        imageURL: 'https://images.prismic.io/wonderbly/f2c493de48bb1501ff672cfd61c7bb55bdd97a7f_0-3.nav.png',
        title: 'Where Are You ...?',
        path: '/#',
      },
    ],
  },
  {
    name: 'Bestsellers',
    icon: {
      name: 'globe',
      color: 'green',
    },
    children: [
      {
        imageURL: 'https://images.prismic.io/wonderbly/c43379f9-3ad1-4697-8087-5b9b3db159f0_2020_Best_Sellers_Nav.png',
        title: 'Where Are You ...?',
        desc: 'Our personalized search-and-find books',
        path: '/#',
      },
      {
        imageURL: 'https://images.prismic.io/wonderbly/c43379f9-3ad1-4697-8087-5b9b3db159f0_2020_Best_Sellers_Nav.png',
        title: 'Where Are You ...?',
        desc: 'Our personalized search-and-find books',
        path: '/#',
      },
      {
        imageURL: 'https://images.prismic.io/wonderbly/c43379f9-3ad1-4697-8087-5b9b3db159f0_2020_Best_Sellers_Nav.png',
        title: 'Where Are You ...?',
        desc: 'Our personalized search-and-find books',
        path: '/#',
      },
    ],
  },
  {
    name: 'Father Day',
    icon: {
      name: 'film',
      color: 'orange',
    },
    path: '/#',
  },
]
export default {
  menus,
}
